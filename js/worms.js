var camera, scene, renderer;
var controls;

var controlsEnabled = false;

var prevTime = performance.now();

var isosurfSz = 128;
var isosurf = [];

var chunk = [];
var chunkSz = 16;
var cantChunks = Math.floor(isosurfSz / chunkSz);
var dirtyChunks = [];

var terrain;

var stats;

init();
animate();

function init() {

  stats = new Stats();
  stats.showPanel( 0 ); // 0: fps, 1: ms, 2: mb, 3+: custom
  document.body.appendChild( stats.dom );

  camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 1, 1000 );

  scene = new THREE.Scene();
  scene.fog = new THREE.Fog( 0xffffff, 0, 750 );

  var light = new THREE.HemisphereLight( 0xeeeeff, 0x777788, 0.75 );
  light.position.set( 10, 10, 10 );
  scene.add( light );

  controls = new THREE.PointerLockControls( camera );

  scene.add( controls.getObject() );

  terrain = new Terrain();
  terrain.init( scene );

  renderer = new THREE.WebGLRenderer();
  renderer.setClearColor( 0xffffff );
  renderer.setPixelRatio( window.devicePixelRatio );
  renderer.setSize( window.innerWidth, window.innerHeight );
  document.body.appendChild( renderer.domElement );

  //

  window.addEventListener( 'resize', onWindowResize, false );

}

function onWindowResize() {

  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize( window.innerWidth, window.innerHeight );

}

function animate() {

  requestAnimationFrame( animate );
  stats.begin();

  if ( controlsEnabled ) {
    var time = performance.now();
    var delta = ( time - prevTime ) / 1000;

    controls.update( delta );

    prevTime = time;
  }

  stats.end();
  renderer.render( scene, camera );
}

function destroy() {
  let pos = controls.getObject().getWorldPosition();
  let dir = new THREE.Vector3(0, 0, -1);
  camera.getWorldDirection(dir);
  dir.normalize();

  let raycaster = new THREE.Raycaster(pos, dir, 0, 1000);
  let hits = raycaster.intersectObjects( scene.children );
  if(hits[0] !== undefined)
    terrain.update( hits[0].point );
}
