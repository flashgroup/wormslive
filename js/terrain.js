
Terrain = function () {

  this.isosurf = [];
  this.isosurfSz = 128;

  this.chunk = [];
  this.chunkSz = 16;
  this.cantChunks = Math.floor(isosurfSz / chunkSz);
  this.dirtyChunks = [];

  this.scene;

  this.init = function( scene ) {
    this.scene = scene;
    createIso();
    createWorld();
  }

  this.update = function( pos ) {
    let xi = Math.floor(pos.x);
    let yi = Math.floor(pos.y);
    let zi = Math.floor(pos.z);

    updateIso(xi, yi, zi, 7);
    updateChunks();
  }
}

function createIso() {
  // Creo la isosuperficie
  for(var x = 0; x < this.isosurfSz; x++) {
  for(var y = 0; y < this.isosurfSz; y++) {
  for(var z = 0; z < this.isosurfSz; z++) {

    
    let dist = new THREE.Vector3(x, y, z).distanceTo(new THREE.Vector3(this.isosurfSz/2, this.isosurfSz/2, this.isosurfSz/2));
    let val = 1;
    if(dist < this.isosurfSz / 4)
      val = -1;
    
    if(y < 60) {
      val = -1;
    }
    

    let index = getIndex3d(x, y, z, this.isosurfSz);
    this.isosurf[index] = val;
  } } }

  // Creo los chunks
  for(var cx = 0; cx < this.cantChunks; cx++) {
  for(var cy = 0; cy < this.cantChunks; cy++) {
  for(var cz = 0; cz < this.cantChunks; cz++) {
    let index = getIndex3d(cx, cy, cz, this.cantChunks);
    this.chunk[index] = new THREE.Object3D();
    this.chunk[index].position.set(cx * this.chunkSz, cy * this.chunkSz, cz * this.chunkSz);
  } } }

}

function createWorld() {
  for(var cx = 0; cx < this.cantChunks; cx++) {
  for(var cy = 0; cy < this.cantChunks; cy++) {
  for(var cz = 0; cz < this.cantChunks; cz++) {
    let index = getIndex3d(cx, cy, cz, this.cantChunks);

    let boundFrom = this.chunk[index].position.clone();
    let boundTo = this.chunk[index].position.clone().add(new THREE.Vector3(this.chunkSz, this.chunkSz, this.chunkSz));

    var world = surfaceNets([this.chunkSz+2, this.chunkSz+2, this.chunkSz+2], function(x,y,z) {

      if(x >= 0 && x < this.isosurfSz && y >= 0 && y < this.isosurfSz && z >= 0 && z < this.isosurfSz) {
        let index = getIndex3d(x, y, z, this.isosurfSz);
        return this.isosurf[index];
      } else {
        return 1;
      }
    },
    [ [boundFrom.x-1, boundFrom.y-1, boundFrom.z-1], [boundTo.x+1, boundTo.y+1, boundTo.z+1] ] );

    var geom = new THREE.Geometry();
    for(let i = 0; i < world.positions.length; i++) {
      var v = world.positions[i];
      var vert = new THREE.Vector3(v[0], v[1], v[2]);
      geom.vertices.push(vert);
    }

    for(let i = 0; i < world.cells.length; i++) {
      var c = world.cells[i];
      var face = new THREE.Face3(c[0], c[1], c[2]);
      geom.faces.push(face);
    }

    geom.computeFaceNormals();

    this.chunk[index].mesh = new THREE.Mesh( geom, new THREE.MeshNormalMaterial() );
    this.scene.add(this.chunk[index].mesh);
  } } }
}

function updateIso(x, y, z, rad) {
  for(var xx = -rad; xx < rad; xx++) {
  for(var yy = -rad; yy < rad; yy++) {
  for(var zz = -rad; zz < rad; zz++) {
    let px = x + xx;
    let py = y + yy;
    let pz = z + zz;

    let indexiso = getIndex3d(px, py, pz, this.isosurfSz);
    if(new THREE.Vector3(px, py, pz).distanceTo(new THREE.Vector3(x, y, z)) < rad ) {
      this.isosurf[indexiso] = 1;
    }

    // chunk sucio
    let xChunk = Math.floor(px / this.chunkSz);
    let yChunk = Math.floor(py / this.chunkSz);
    let zChunk = Math.floor(pz / this.chunkSz);
    if(xChunk >= 0 && xChunk < this.cantChunks && yChunk >= 0 && yChunk < this.cantChunks && zChunk >= 0 && zChunk < this.cantChunks) {

      let index = getIndex3d(xChunk, yChunk, zChunk, this.cantChunks);

      if( this.dirtyChunks.indexOf(index) === -1 ) {
        this.dirtyChunks.push(index);
      }
    }
  }
  }
  }
}

function updateChunks() {
  for(let i = 0; i < this.dirtyChunks.length; i++) {
    let index = this.dirtyChunks[i];
    this.scene.remove(this.chunk[index].mesh);

    let boundFrom = this.chunk[index].position.clone();
    let boundTo = this.chunk[index].position.clone().add(new THREE.Vector3(this.chunkSz, this.chunkSz, this.chunkSz));

    var world = surfaceNets([this.chunkSz+2, this.chunkSz+2, this.chunkSz+2], function(x,y,z) {

      let indexiso = getIndex3d(x, y, z, this.isosurfSz);
      return this.isosurf[indexiso];
    },
    [ [boundFrom.x-1, boundFrom.y-1, boundFrom.z-1], [boundTo.x+1, boundTo.y+1, boundTo.z+1] ] );

    var geom = new THREE.Geometry();

    for(let perro = 0; perro < world.positions.length; perro++) {
      var v = world.positions[perro];
      var vert = new THREE.Vector3(v[0], v[1], v[2]);
      geom.vertices.push(vert);
    }

    for(let gato = 0; gato < world.cells.length; gato++) {
      var c = world.cells[gato];
      var face = new THREE.Face3(c[0], c[1], c[2]);
      geom.faces.push(face);
    }

    geom.computeFaceNormals();

    this.chunk[index].mesh = new THREE.Mesh( geom, new THREE.MeshNormalMaterial() );
    this.scene.add(this.chunk[index].mesh);
  }

  this.dirtyChunks = [];
}

function getIndex3d(x, y, z, sz) {
  return x + sz * (y + sz * z);
}
